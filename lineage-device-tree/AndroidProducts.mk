#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlusNordCE.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlusNordCE-user \
    lineage_OnePlusNordCE-userdebug \
    lineage_OnePlusNordCE-eng
